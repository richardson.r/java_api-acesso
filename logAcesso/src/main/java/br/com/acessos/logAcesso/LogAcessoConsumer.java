package br.com.acessos.logAcesso;

import br.com.acessos.acesso.model.AcessoLog;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class LogAcessoConsumer {
    @KafkaListener(topics = "spec4-richardson-ramiro-1", groupId = "xinforimpola")
    public void receber(@Payload AcessoLog acessoLog) {
        String permissao = "negado";
        if (acessoLog.isPermissao())
            permissao = "permitido";

        System.out.println("Acesso a porta(" + acessoLog.getPortaId() + ") " + permissao + " ao cliente(" + acessoLog.getClienteId() + ")");
    }
}
