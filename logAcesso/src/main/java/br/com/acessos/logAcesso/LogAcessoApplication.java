package br.com.acessos.logAcesso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogAcessoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogAcessoApplication.class, args);
	}

}
