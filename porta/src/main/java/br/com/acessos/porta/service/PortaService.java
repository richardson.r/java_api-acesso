package br.com.acessos.porta.service;

import br.com.acessos.porta.exceptions.PortaNaoEncontradaException;
import br.com.acessos.porta.model.Porta;
import br.com.acessos.porta.repository.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta create(Porta porta){
        return portaRepository.save(porta);
    }

    public Porta getById(Long id){
        return portaRepository.findById(id)
                .orElseThrow(PortaNaoEncontradaException::new);
    }
}
