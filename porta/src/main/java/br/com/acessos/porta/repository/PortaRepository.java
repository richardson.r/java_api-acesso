package br.com.acessos.porta.repository;

import br.com.acessos.porta.model.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository<Porta, Long> {

}
