package br.com.acessos.porta.controller;

import br.com.acessos.porta.model.Porta;
import br.com.acessos.porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @GetMapping("/{id}")
    public Porta getById(@PathVariable Long id){
        return portaService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta create(@RequestBody Porta porta){
        porta = portaService.create(porta);
        return porta;
    }
}
