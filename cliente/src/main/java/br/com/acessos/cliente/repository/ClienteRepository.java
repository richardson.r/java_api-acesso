package br.com.acessos.cliente.repository;

import br.com.acessos.cliente.model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {
}
