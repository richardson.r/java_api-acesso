package br.com.acessos.cliente.service;

import br.com.acessos.cliente.exceptions.ClienteNaoEncontradoException;
import br.com.acessos.cliente.model.Cliente;
import br.com.acessos.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente create(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente getById(Long id){
        return clienteRepository.findById(id)
                .orElseThrow(ClienteNaoEncontradoException::new);
    }
}
