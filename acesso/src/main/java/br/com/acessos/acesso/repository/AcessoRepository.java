package br.com.acessos.acesso.repository;

import br.com.acessos.acesso.model.Acesso;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository<Acesso, Long> {
    Optional<Acesso> findByClienteIdAndPortaId(Long cliente_id, Long porta_id);
}
