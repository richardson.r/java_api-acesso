package br.com.acessos.acesso.controller;

import br.com.acessos.acesso.exceptions.AcessoNaoEncontradoException;
import br.com.acessos.acesso.model.Acesso;
import br.com.acessos.acesso.service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @GetMapping("/{clienteId}/{portaId}")
    public Acesso getByClienteIdPortaId(@PathVariable Long clienteId, @PathVariable Long portaId) {
        return acessoService.getAcesso(clienteId, portaId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Acesso create(@RequestBody @Valid Acesso acesso) {
        acesso = acessoService.create(acesso);
        return acesso;
    }

    @DeleteMapping("/{clienteId}/{portaId}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable @Valid Long clienteId, Long portaId) {

    }


}
