package br.com.acessos.acesso.service;

import br.com.acessos.acesso.exceptions.AcessoNaoEncontradoException;
import br.com.acessos.acesso.model.Acesso;
import br.com.acessos.acesso.model.AcessoLog;
import br.com.acessos.acesso.repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private KafkaTemplate<String, Acesso> producer;

    public Acesso create(Acesso acesso){
        return acessoRepository.save(acesso);
    }

    public Acesso getById(Long id){
        return acessoRepository.findById(id)
                .orElseThrow(AcessoNaoEncontradoException::new);
    }

    public Acesso getAcesso(Long clienteId, Long portaId){
        Optional<Acesso> acesso = acessoRepository.findByClienteIdAndPortaId(clienteId, portaId);

        AcessoLog acessoLog = new AcessoLog();
        acessoLog.setClienteId(clienteId);
        acessoLog.setPortaId(portaId);

        if(!acesso.isPresent()) {
            acessoLog.setPermissao(true);
            producer.send("spec4-richardson-ramiro-1", acesso.get());
            return acesso.get();
        }else {
            acessoLog.setPermissao(false);
            producer.send("spec4-richardson-ramiro-1", acesso.get());
            throw new AcessoNaoEncontradoException();
        }
    }
}
